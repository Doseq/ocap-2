export const App = {
	AUTHOR: 'MisterGoodson',
	BI_THREAD_URL: 'https://forums.bohemia.net/forums/topic/194164-ocap-op-capture-and-playback-aarreplay/',
	COMMUNITY_TITLE: '3 Commando Brigade',
	COMMUNITY_URL: 'https://www.3commandobrigade.com',
	GITHUB_URL: 'https://github.com/mistergoodson/OCAP',
	TITLE_FULL: 'Operation Capture And Playback',
	TITLE: 'OCAP',
	VERSION: '1.0',
}

export const CharCode = {
	SPACE: 32,
	E: 101,
	R: 114,
};

export const ElementId = {
};

export const ClassName = {
	EVENT_DETAILS: 'event-details',
	EVENT_TIMELINE_TICK: 'tick',
	EVENT: 'event',
	IN_PROGRESS: 'in-progress',
	PAUSE: 'paused',
	PROGRESS_BAR_CONTAINER: 'progress-bar-container',
	PROGRESS_BAR: 'progress-bar',
	REVEAL: 'reveal',
	SHOW: 'show',
};

export const CAPTURES_PATH = '/static/captures';
export const MAP_META_FILENAME = 'meta.json';
export const MAPS_PATH = '/static/images/maps';
export const MARKERS_PATH = '/static/images/markers';