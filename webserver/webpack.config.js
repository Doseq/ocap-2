module.exports = {
	entry: './js/app.js',
	output: {
		path: __dirname + '/static/scripts',
		filename: 'ocap.js'
	},
	devtool: 'source-map'
};